'use strict';

function qcolour(level) {
  switch (level) {
    case 0:
     return "#2ecb0d";
    case 1:
    case 2:
    case 3:
     return "darkgreen"
    case 4:
      return "orange"
    case 5:
      return "darkorange";
    case 6:
    case 7:
      return "darkred";
  }
}

function includeBTS(value) {
    DATA_INCLUDE_BTS=value;
}

function includeChanType(value) {
    DATA_INCLUDE_CHAN=value;
}

function paramMaxItems(new_value) {
    DATA_MAX_IMSI = parseInt(new_value);
}

function paramDTO(new_value) {
    MEAS_PURGE_TIMEOUT = parseInt(new_value);
}

function paramSortBy(new_value) {
  d3.select('#' + SORT_ITEMS_BY)
    .classed('sortby-active', false)
    .classed('sortby-inactive', true);

  d3.select('#' + new_value)
    .classed('sortby-active', true)
    .classed('sortby-inactive', false);

  SORT_ITEMS_BY = new_value; //'age' or 'imsi'
  sort_data();
}

function setSelected(imsi) {
  d3.select('#imsi-' + selected_imsi)
    .classed('row-selected', false);
  selected_imsi = imsi;
  d3.select('#imsi-' + selected_imsi)
    .classed('row-selected', true);
}

function format_chan_id(d,small) {
  var bts_index = d.chan_info.bts_nr;
  if (bts_index > bts_defs.length-1  ) {
    return "UnKnown-"+bts_index+"-"+d.chan_info['trx_nr']+"-"+d.chan_info['ts_nr'];
  }
  if (small) {
    return d.chan_info['bts_nr']+"/"+d.chan_info['trx_nr']+"<br />"+d.chan_info['ts_nr'];
  }
  var chan_id = "<span style='color:red;font-weight:bold'>"+bts_defs.find(e => e[0] == bts_index)[2]+
                "</span><br />BTS["+d.chan_info['bts_nr']+
                "]-<span style='color:darkgreen'>TRX" +d.chan_info['trx_nr']
                +"</span><br />"+
                "Timeslot "+d.chan_info['ts_nr']+"";

  // There is only one sub-slot in TCH/F
  if (d.chan_info['lchan_type'].indexOf("TCH/F") == -1)
    chan_id += "<br />Subslot "+d.chan_info['ss_nr']+"";

  return chan_id
}

function show_last_time(t) {
    var date = new Date(t)
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    $('#last_time').html(formattedTime);
}

function update_counters(info) {

    let b = info.bts_nr
    switch (info.lchan_type.substring(0,3)) {
      case "TCH":
        $('#chan_seen_t_'+b).html(++chan_seen_t[b]);
        break;
      case "SDC":
        $('#chan_seen_s_'+b).html(++chan_seen_s[b]);
        break;
      default:
        $('#chan_seen_o_'+b).html(++chan_seen_o[b]);
        break;
    }
}

function update_meters(bts_nr, time) {
  let s = count_current(bts_nr, time, 'SDCCH')
  let t = count_current(bts_nr, time, 'TCH')
  $('#meter_sdcch_'+bts_nr).val(s).attr('title','SDCCH: ('+s+')')
  $('#meter_tch_'+bts_nr).val(t).attr('title','TCH: ('+t+')')
  $('#SN'+bts_nr).attr('title', 'SDCCH:'+s+' / TCH:'+t)

}
function update_current(time, info) {
  let bts = info.bts_nr
  let trx = info.trx_nr
  let ts  = info.ts_nr
  let ss  = info.ss_nr
  let ch  = info.lchan_type

  if (!current[bts]) {
    current[bts] = [];
  }
  if (!current[bts][trx]) {
    current[bts][trx] = [];
  }
  if (!current[bts][trx][ts]) {
    current[bts][trx][ts] = [];
    current[bts][trx][ts][ss] = [];
  }
  current[bts][trx][ts][ss] = [ ch, time ];
}

function count_current(bts, time, chan_type) {
  let count = 0
  if (!current[bts]) {
    return 0
  }

  current[bts].forEach((trx) => {
    trx.forEach((ts) => {
      ts.forEach((ss) => {
        if (ss[0].substr(0,chan_type.length) == chan_type && ss[1] > (time - 3)) {  count++ }
      })
    })
  })
  return count
}

function try_get_name(msisdn) {
  $.ajax( {
      type: 'GET',
      url: rapi+"subscriber/name/"+msisdn,
      dataType: 'json',
      success: function( data ) {
        if (data.status == "failed") {
          subscribers[msisdn] = "UnKnown"
        } else {
          subscribers[msisdn] = data
        }},
      data: {},
      async: false
  });
}

function get_msisdn(imsi) {
  $.ajax( {
      type: 'GET',
      url: rapi+"subscriber/extension/"+imsi+'?min_length=11',
      dataType: 'json',
      success: function( data ) {
        if (data.status == "failed") {
          imsi_msisdn[imsi] = "UnKnown"
        } else {
          imsi_msisdn[imsi] = data
        }},
      data: {},
      async: false
  });
}

function update_data(new_row) {

  /* Called each time we get a new line on the websocket
     Followed by sort,trim and then update_reports() */
  
  show_last_time(new_row.time * 1000)
  
  if (!(new_row && new_row.meas_rep &&
         (new_row.meas_rep.DL_MEAS || new_row.meas_rep.UL_MEAS))) {
    // No report
    return;
  }

  var report=new_row.meas_rep;
  var info=new_row.chan_info;
  var NR_old;
  var NR_new;
  var NR_rolled;
  var _ci;
  let b = info.bts_nr

  if (report.NUM_NEIGH > max_neigh ) { max_neigh = report.NUM_NEIGH; }
  if (b > max_bts) { max_bts = b }

  rp = info

  var cur_idx = null;
  var chan_seen = 0;
  update_current(new_row.time, info)
  update_meters(info.bts_nr, new_row.time)

  if (new_row['imsi'] == '') {
      _ci = new_row['chan_info']
      //new_row['imsi'] = "UnKnown-" + _ci['bts_nr'] + "-" + _ci['trx_nr'] + "-" +  _ci['ts_nr'] + "-" + _ci['ss_nr']
      new_row['imsi'] = new_row['name'];
  } else {
    // The BSC knows the IMSI
    var imsi = new_row['imsi']
    if (!(imsi in imsi_msisdn)) {
      get_msisdn(imsi);
    }
    if (imsi_msisdn[imsi] && ( new_row['name'] == '' || new_row['name'].indexOf("subscr-IMSI-"+imsi) != -1 ) ) {
      new_row['name'] = (subscribers[imsi_msisdn[imsi]]) ?
                      subscribers[imsi_msisdn[imsi]] + "<br>\n" + "[" + imsi_msisdn[imsi] + "]" :
                      try_get_name(imsi_msisdn[imsi]) + "[" + imsi_msisdn[imsi] + "]";
    }
  }

  //data.forEach(function(d, i) {
  for (const [i, d] of data.entries()) {
    if (new_row['imsi'] === d['imsi']) {
      cur_idx = i;
      break;
    }
  };

  for (var i=0; i<seen.length; i++ ) {
    if (seen[i][0] == new_row.imsi &&
        seen[i][1] == info.lchan_type &&
        (new_row.meas_rep.NR > 0 || seen[i][2] > new_row.time-3)) {
      /* We saw a meas report for this channel imsi/type combination
       * within the last three seconds. Or we saw it at some point and it
       * is not new (NR > 0) This is vulnerable to lost meas reports but it
       * not critical. */
      chan_seen = 1
      seen[i][2] = new_row.time
      break;
    }
  }
  if (chan_seen == 0) {
    seen.push(Array(new_row.imsi, info.lchan_type, new_row.time));
    update_counters(info)
  }

  if (DATA_INCLUDE_BTS != '_' && DATA_INCLUDE_BTS != info.bts_nr ) {
    return;
  }
  if (DATA_INCLUDE_CHAN != '_' && info.pchan_type.indexOf(DATA_INCLUDE_CHAN) === -1 ) {
    return;
  }

  ['DL_MEAS', 'UL_MEAS'].forEach(function(link_type) {
    if (!report[link_type]) { report[link_type] = default_values; }
  });

  new_row['age'] = 0;
  new_row.active = true;
  new_row.purge = false;
  if (cur_idx === null) {
    // This is the first report we see for this channel
    new_row['first_report'] = new_row['time'];
    new_row['duration'] = 0;
    data.push(new_row);
  } else {
    NR_old = data[cur_idx]['meas_rep']['NR'];
    NR_new = new_row['meas_rep']['NR'];
    // HACK: Guess old channel vs new chanel by NR.
    //       NR rolls at 255, plus some reports may be lost,
    //       so we can't detect new channels reliably.
    //       A proper way of doing this would be to pass some
    //       kind of channel ID in the meas_report structure.
    NR_rolled = NR_new < 2 && NR_old > 253;
    if (NR_old > NR_new && !NR_rolled) {
      new_row['first_report'] = new_row['time'];
    } else {
      new_row['first_report'] = data[cur_idx]['first_report'];
    }
    new_row['duration'] = new_row['time']-new_row['first_report'];
    data[cur_idx] = new_row;
  }
  };

function sort_data() {
  data.sort(function(a,b) {

    if (a.active && !b.active) {
      return -1;
    }
    if (!a.active && b.active) {
      return 1;
    }

    return d3.ascending(b.duration,a.duration);

    /*
    if (a.active && b.active) {
      if (a.first_report === b.first_report) {
        return d3.ascending(a.imsi, b.imsi);
      } else {
        return d3.descending(a.first_report, b.first_report);
      }
    }
    if (!a.active && !b.active) {
      if (SORT_ITEMS_BY === 'age') {
        if (a.time === b.time) {
          return d3.ascending(a.imsi, b.imsi);
        } else {
          return d3.descending(a.time, b.time);
        }
      } else {
        return d3.ascending(a.imsi, b.imsi);
      }
    } */
  });
}

function trim_data() {
  data = data.slice(0, DATA_MAX_IMSI);
  data = data.filter(function(i) {return !i.purge} );
}

function key(d) {
  return d.imsi;
}

function update_reports() {
  var imsis = d3.select(".meas")
    .selectAll(".data-row")
    .data(data, key)

  imsis.enter().call(format_data_row);

  imsis.each(update_data_row);

  imsis.exit().remove();
}

function update_data_row(el, i) {

  // Called from update reports.

  let L;
  let Q;
  let AQ;
  var A = '';
  var B = '';
  let unit = 'px';
  let bar_w;
  let scale_f;
  let small = window.matchMedia('(max-width: 600px)').matches;

  var cur_el = d3.select(this)
    .classed("data-row-active", function(d) { return d.active; })
    .classed("data-row-inactive", function(d) { return !d.active; })
    .classed("row-selected", function(d) { return selected_imsi === d.imsi; })
    .attr("id", function(d) { return 'imsi-' + d.imsi; })
    .on("click", function(d,i) { setSelected(d.imsi); });

  ['name', 'imsi', 'duration', 'age'].forEach(function(id) {
    cur_el.select('#meas-' + el.imsi + '-' + id)
      .html(function(d) { return el[id]; })
  });

  cur_el.select("#meas-" + el.imsi + "-chan-id")
    .html(function(d) { return d['chan_info']?format_chan_id(d,small):""; });

  cur_el.select("#meas-" + el.imsi + "-chan-type")
    .text(function(d) { return d['chan_info']?d.chan_info["pchan_type"]:""; });

  cur_el.select("#meas-" + el.imsi + "-q")
    .html(function(d) { return d.meas_rep['UL_MEAS']['RXQ-FULL'] +
                               "<br/>" +
                               d.meas_rep['DL_MEAS']['RXQ-FULL']});

  ['L1_MS_PWR', 'L1_TA', 'NR'].forEach(function(id) {
    cur_el.select("#meas-" + el.imsi + "-" + id.toLowerCase())
      .text(function(d) { return el.meas_rep[id]; })
  });

  cur_el.select("#meas-" + el.imsi + "-neigh")
      .text(function(d) { return d.meas_rep['NUM_NEIGH']; });


  ['UL', 'DL'].forEach(function(k) {
    L = el.meas_rep[k+'_MEAS']['RXL-FULL'];
    Q = el.meas_rep[k+'_MEAS']['RXQ-FULL'];
    if (qavg[k][el.imsi] === undefined) { qavg[k][el.imsi] = [] }
    if (qavg[k][el.imsi].length > 50) { qavg[k][el.imsi].shift() }
    qavg[k][el.imsi].push(Q);
    AQ = Math.round(d3.mean(qavg[k][el.imsi]));


    if (small) {
      scale_f = xScale_percent
      unit = '%'
    } else {
      scale_f =xScale
      unit = 'px'
    }

    if (L != MINPWR) {
      bar_w = Math.abs(Math.round(scale_f(L)))
      let bar_left = (bar_w-52) * qxScale(AQ)/100
      if (bar_left < 0 ) { bar_left = 0 }

      if (!small) {
        cur_el.select("#meas-" + el.imsi + "-Q-"+k)
          .interrupt()
          .transition()
          .duration(200)
          .style("left", bar_left+'px')
          .style("width", function(d) { return ((AQ == 7) ? bar_w-20 : AQ + (60 * qxScale(AQ)/100)) + 'px' })
          .style("background-color", function() { return qcolour((AQ)); })
          .style("opacity", function() { return (AQ == 0) ? 0.4 : 1-(qxScale(AQ)/100)/2 })
          .style("margin-top", function() { return ((AQ == 7) ? -20 : -7) + 'px' } )
          .style("height", function() { return ((AQ == 7) ? 20 : 7) + 'px' } )
      }

      cur_el.select("#meas-" + el.imsi + "-L-"+k)
        .classed("meas-inactive", false)
        .classed("sdcch", el.chan_info.lchan_type == "SDCCH")
        .classed("tch", el.chan_info.lchan_type != "SDCCH")
        .interrupt()
        .transition()
        .duration(el.age === 0 ? 250 : 70)
        .style("width", bar_w + unit)
        .text(function(d) { return L; });
    } else {

      cur_el.select("#meas-" + el.imsi + "-L-"+k)
        .classed("meas-inactive", true)
    }

  });

  if (small) {
    return
  }

  if (el.meas_rep['NEIGH'] != undefined &&
      (el.meas_rep['NUM_NEIGH'] != el.meas_rep['NEIGH'].length)) {
    console.error("neighbour report length mismatch")
  }

  var bar_tr = cur_el.select("#meas-" + el.imsi + "-level")

  if (el.meas_rep['NEIGH'] != undefined && el.meas_rep['NEIGH'].length > 0) {

    for (i=0; i <= max_neigh-1; i++) {
      L = '--';
      A = '';
      B = '?';
      if (el.meas_rep['NEIGH'][i] != undefined) {
        L = el.meas_rep['NEIGH'][i]['POWER'];
        A = el.meas_rep['NEIGH'][i]['ARFCN'];
        B = el.meas_rep['NEIGH'][i]['BSIC'];
      }

      if (L != '--' && bar_tr.select('#neighbour-' + el.imsi + '-' + i).empty()) {
           bar_tr.append("div")
            .attr("class", "neighbour bar")
            .attr("id", "neighbour-" + el.imsi + "-" +i)
            .style("display", "block")
      }

      bar_tr.select('#neighbour-' + el.imsi + '-' + i)
        .html(function(d,i,nodes) { return neighbour_label(A, B, L, nodes[i]); })
        .style("display", function(d) { return (d.active) ? "block" : "none" })
        .classed("meas-inactive", L == '--')
        .interrupt()
        .transition()
        .style("width", function(d) {return Math.abs(Math.round(scale_f(L))) + unit});
    }

  } else {
     bar_tr.selectAll('.neighbour').classed("meas-inactive", true)
  }

}

function neighbour_label(A, B, L, node) {
    if (L == '--') {
      return node.innerHTML;
    }
    for (const [i, ti, n, a, b] of bts_defs) {
      if (a == A && b == B) {
        return "<span class='arfcn'>"+n+"</span>"+" <span class='level'>"+L+"</span>";
      }
    }
    if (typeof(extra_neigh_info) === "undefined") {
      return "<span class='arfcn'>B: "+B+" A: "+A+"</span>"+" <span class='level'>"+L+"</span>";
    }
    for (const [n, a, b] of extra_neigh_info) {
      if (a == A && b == B) {
        return "<span class='arfcn'>"+n+"</span>"+" <span class='level'>"+L+"</span>";
      }
    }
    return "<span class='arfcn'>B: "+B+" A: "+A+"</span>"+" <span class='level'>"+L+"</span>";
}

function format_data_row(data_row) {

  let i;

  if (data_row.data().length == 0) {
    return;
  }

  let imsi = data_row.data()[0].imsi
  var tr = data_row.append("div")
    .attr("class", "data-row");

  ['name', 'imsi', 'chan-type', 'chan-id', 'duration', 'age', 'NR', 'direction',
   'Q', 'level', 'L1_MS_PWR', 'L1_TA', 'NEIGH',].forEach(function(k) {
    tr.append("div")
      .attr("class", "cell")
      .attr("id", "meas-" + imsi + "-" + k.toLowerCase());
  });

  tr.select("#meas-" + imsi + "-chan-type").attr("class", "cell chan-type");
  tr.select("#meas-" + imsi + "-direction").html("UL<br/>DL");

  var bar_tr = tr.select("#meas-" + imsi + "-level")
    //.attr("style", 'width: ' + (MAXW + 20) + 'px')
    .classed("chart", true);

  ['UL', 'DL'].forEach(function(k) {
    bar_tr.append("div")
      .attr("class", (k === 'UL') ? "uplink bar" : "downlink bar")
      .attr("id", "meas-" + imsi + "-L-"+k);
    //bar_tr.append("div").attr("class", "qcon").html('<div class="qmark"></div>')
    bar_tr.append("div")
      .attr("class", (k === 'UL') ? "quality uplink bar" : "quality downlink bar")
      .attr("id", "meas-" + imsi + "-Q-"+k)
  });

  /*
  for (i=0; i <= max_neigh; i++) {

    bar_tr.append("div")
      .attr("class", "neighbour bar")
      .attr("id", "neighbour-" + imsi + "-" +i)
      .style("display", "none")
  }
  tr.style("height", 80+(max_neigh-1*12)+'px');*/
  return true;
}

function refresh_data() {
  /* Called on interval (1second) */
  data.forEach(function(v, i) {
    data[i]['age'] += 1;
    if (data[i]['age'] > MEAS_TIMEOUT) {
      data[i]['active'] = false;
    }
    if (data[i]['age'] > MEAS_PURGE_TIMEOUT) {
      qavg.UL[data[i]['imsi']] = [];
      qavg.DL[data[i]['imsi']] = [];
      data[i]['purge'] = true;
    }
  });

  return true;
}


