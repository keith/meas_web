# Web version of the meas_vis utility for osmo-bsc and osmo measurement utils

This application offers real-time view of GSM measurement reports generated by the OsmoBSC. It utilizes `meas_json` utility which takes the measurement reports feed and converts it to JSON.

![Screenshot](meas_web_screenshot.png?raw=true)

## Installation

1. install websocketd (https://github.com/joewalnes/websocketd/releases)
2. add to /etc/osmocom/osmo-bsc.cfg file
```
network
 meas-feed destination 127.0.0.1 8888
```
3. Add etc/sv/* to runit configuration
4. open http://host.name:8080/index.html in your browser
